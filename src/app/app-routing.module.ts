import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPokemonsComponent } from './components/list-pokemons/list-pokemons.component';
import { ViewPokemonComponent } from './components/view-pokemon/view-pokemon.component';


const routes: Routes = [
  {
    path:'',
    component:ListPokemonsComponent
    
  },
  {
    path:'pokemon:name',
    component:ViewPokemonComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
