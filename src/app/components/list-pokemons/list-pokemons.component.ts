import { Component, OnInit } from '@angular/core';
//import { ConsoleReporter } from 'jasmine';
import {PokemonService} from '../../services/pokemons.service'
import { FormControl } from '@angular/forms';
import {
  Router
} from "@angular/router";



@Component({
  selector: 'app-list-pokemons',
  templateUrl: './list-pokemons.component.html',
  styleUrls: ['./list-pokemons.component.css'],
  providers: [PokemonService]
})
export class ListPokemonsComponent implements OnInit {
  pokemons;
  pokemon = new FormControl('');
  pokemonBuscar;
  isPokemon;
  message: string;
  constructor(private _pokemonService:PokemonService,private router: Router) { }

  ngOnInit(): void {
    console.log("Work");
    this.buscar();
    
  }

  buscar(){
    let pokemondata = this.pokemon.value;
    this.pokemonBuscar = pokemondata;
    this._pokemonService.getPokemons().subscribe((responseBody) => {
      
      this.pokemons = 
      responseBody['results'].filter(
      pokemon => pokemon.name == this.pokemonBuscar);
      if(this.pokemons == ""){
        this.message = "No hay resultados";
      }
      else{
        this.message = "";
      }

      });
      
    this.isPokemon = false;
  }
  mostrarpokemon(pokemon){
    this.pokemonBuscar = pokemon;
    console.log(this.pokemonBuscar);
    this.isPokemon = true;
  }  
}
