import { Component, Input, OnInit } from '@angular/core';
import {PokemonService} from '../../services/pokemons.service'

@Component({
  selector: 'app-view-pokemon',
  templateUrl: './view-pokemon.component.html',
  styleUrls: ['./view-pokemon.component.css'],
  providers: [PokemonService]
})
export class ViewPokemonComponent implements OnInit {
  @Input()
  pokemon;
  constructor(private _pokemonService:PokemonService) { }

  ngOnInit(){
    console.log(this.pokemon);
    this.mostrarpokemon(this.pokemon);
  }
  
  mostrarpokemon(pokemon){
    this._pokemonService.getPokemon(pokemon).subscribe((responseBody) => {
      this.pokemon = responseBody;
      console.log(this.pokemon);
    });
  }

}
