import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) { }

  getPokemons() {
    return this.httpClient.get("https://localhost:5001/api/pokemon");
  }
  getPokemon(pokemon) {
    return this.httpClient.get("https://localhost:5001/api/pokemon/"+ pokemon);
  }
}
